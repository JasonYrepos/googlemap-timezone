import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web
import json
import urllib2
import time


class WSH(tornado.websocket.WebSocketHandler):
  connections = set()
  
  def check_origin(self, origin):
    return True
    
  def open(self):
    self.connections.add(self)
    print 'Connected.\n'

  def on_message(self, message):
    print '%s\n' %message
    for con in self.connections:
        con.write_message(message)
        
	
    #self.write_message(message + ' OK')

  def on_close(self):
    self.connections.remove(self)
    print 'Disconnected\n'

application = tornado.web.Application([(r'/ws', WSH),])

if __name__ == "__main__":

  http_server = tornado.httpserver.HTTPServer(application)
  http_server.listen(8888)
  tornado.ioloop.IOLoop.instance().start()

